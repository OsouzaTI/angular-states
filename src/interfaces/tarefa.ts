import { ITarefa } from "./tarefa.interface";

class Tarefa implements ITarefa {

    descricao: String = "";

    public setDescricao(value: String) {
        this.descricao = value;
    }

}


export default Tarefa;