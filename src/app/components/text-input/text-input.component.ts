import { Component, Input, OnInit } from '@angular/core';
import Tarefa from 'src/interfaces/tarefa';
import { ITarefa } from 'src/interfaces/tarefa.interface';
import { TarefasService } from 'src/services/tarefas.service';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})
export class TextInputComponent implements OnInit {

  tarefas : Array<Tarefa> = [];

  @Input()
  tarefa : Tarefa = new Tarefa();

  constructor(private tarefaService : TarefasService) {}

  ngOnInit(): void {
    // toda vez que o serviço for solicitado e atualizado
    this.tarefaService.getObservable().subscribe((tarefas) => {
      this.tarefas = tarefas;
      console.log("lista de tarefas atualizada", tarefas);
    });
  }

  addTarefa() {
    const nTarefa = new Tarefa();
    nTarefa.setDescricao(this.tarefa.descricao);

    // adiciona a tarefa na lista
    this.tarefas.push(nTarefa);
    // dispara o evento do serviço
    this.tarefaService.getState().setValue(this.tarefas);
    // limpa campo
    this.clear();
    console.log("tarefa adicionada");
  }

  clear() {
    this.tarefa.descricao = "";
  }

  mostrar(evento : any) {
    console.log(this.tarefa.descricao)
  }

}
