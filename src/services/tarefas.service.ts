import { Injectable } from '@angular/core';
import Tarefa from 'src/interfaces/tarefa';
import { Store } from 'src/utils/angular-states/store';

@Injectable({
  providedIn: 'root'
})
export class TarefasService extends Store<Array<Tarefa>> {

  constructor() {
    super([]);
  }

}
