import { BehaviorSubject, Observable } from "rxjs";
import { State } from "./state";
import IStore from "./interfaces/store.interface";

export class Store<T> implements IStore<T> {

    // Estado
    private storeState: State<T>;

    constructor(private initialState: T) {
        // definindo o estado inicial
        this.storeState = new State(initialState);
    }

    public getObservable() { return this.storeState.getObservable() }
    public getState(){ return this.storeState; }


}